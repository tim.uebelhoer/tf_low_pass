#include <dynamic_reconfigure/server.h>
#include <ros/ros.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_ros/transform_listener.h>
#include <tf_low_pass/tf_low_passConfig.h>

// declaration
class TfLowPass {
 public:
  TfLowPass();

 private:
  ros::NodeHandle nh;
  ros::Timer timer;
  dynamic_reconfigure::Server<tf_low_pass::tf_low_passConfig> server;
  tf2_ros::Buffer tf_buffer;
  tf2_ros::TransformListener tf_listener;
  tf2_ros::TransformBroadcaster tf_broadcaster;

  tf2::Transform last_transform;
  ros::Time last_update;

  double time_constant;
  std::string frame_id, child_frame_id, filtered_frame_id;

  void dynamic_reconfigure_callback(tf_low_pass::tf_low_passConfig &config,
                                    uint32_t level);
  void timer_callback(const ros::TimerEvent &timerEvent);
};

// implementation

TfLowPass::TfLowPass() : tf_listener(tf_buffer) {
  ros::NodeHandle pnh("~");
  pnh.param<std::string>("frame_id", frame_id, "world");
  pnh.param<std::string>("child_frame_id", child_frame_id, "tracked_object");
  pnh.param<std::string>("filtered_frame_id", filtered_frame_id,
                         "filtered_object");
  server.setCallback(std::bind(&TfLowPass::dynamic_reconfigure_callback, this,
                               std::placeholders::_1, std::placeholders::_2));
  ROS_INFO("dynamic reconfigure initialized");
  timer = nh.createTimer(ros::Duration(0.02), &TfLowPass::timer_callback, this);
  time_constant = 0.15;
  ROS_INFO("timer initialized");
}

void TfLowPass::dynamic_reconfigure_callback(
    tf_low_pass::tf_low_passConfig &config, uint32_t level) {
  if (config.time_constant > 0) {
    time_constant = config.time_constant;
  }
  timer.setPeriod(ros::Duration(config.timer_period));
}

void TfLowPass::timer_callback(const ros::TimerEvent &timer_event) {
  try {
    // find transform for the expected timer time (steady intervals)
    geometry_msgs::TransformStamped current_msg, last_msg;
    current_msg =
        tf_buffer.lookupTransform(frame_id, child_frame_id, ros::Time(0));
    tf2::Transform current_transform;
    tf2::fromMsg(current_msg.transform, current_transform);
    // apply exponential smoothing
    if (last_update.toSec() > 0) {
      double delta_t = (current_msg.header.stamp - last_update).toSec();
      if (delta_t > 0) {
        double smoothing_factor = 1 - std::exp(-delta_t / time_constant);
        current_transform.setRotation(last_transform.getRotation().slerp(
            current_transform.getRotation(), smoothing_factor));
        current_transform.setOrigin(last_transform.getOrigin().lerp(
            current_transform.getOrigin(), smoothing_factor));
      }
    }
    // publish the transform
    current_msg.transform = tf2::toMsg(current_transform);
    current_msg.child_frame_id = filtered_frame_id;
    tf_broadcaster.sendTransform(current_msg);
    // update last to current
    last_transform = current_transform;
    last_update = current_msg.header.stamp;
  } catch (tf2::TransformException &ex) {
    ROS_WARN("%s", ex.what());
    ros::Duration(1.0).sleep();
  }
}

main(int argc, char *argv[]) {
  ros::init(argc, argv, "tf_low_pass");
  TfLowPass tf_low_pass;
  ros::spin();
  return EXIT_SUCCESS;
}